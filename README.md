## Synopsis

Functión Breadcrumbs , Migas de pan para Wordpress

## Code Example

function the_breadcrumb() {
		global $post;
		
    $anc = get_post_ancestors( $post->ID );

     if (!is_home())
    	echo "<a href=".home_url('/').">".__('Home')."</a>>";

    for($i = count($anc) - 1; $i >= 0; $i--){
    	if($i== count($anc) -1)
      	echo "<a href=" . get_permalink($anc[$i]) . ">" . get_the_title($anc[$i]) . "</a>";
    	if($i != 1)
    		echo " > ";
    }

    if (!is_home())
    	echo "<a href=" . get_permalink() . ">" . get_the_title() . "</a>";

}  


## Installation

Insertar en el archivo functions.php de tu plantilla y podrás llamarlo en tus archivos o tema hijo

