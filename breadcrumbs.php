function the_breadcrumb() {
    $anc = get_post_ancestors( $post->ID );

     if (!is_home())
    	echo "<a href=".home_url('/').">".__('Home')."</a>>";

    for($i = count($anc) - 1; $i >= 0; $i--){
    	if($i== count($anc) -1)
      	echo "<a href=" . get_permalink($anc[$i]) . ">" . get_the_title($anc[$i]) . "</a>";
    	if($i != 1)
    		echo " > ";
    }

    if (!is_home())
    	echo "<a href=" . get_permalink() . ">" . get_the_title() . "</a>";
}